// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'

import App from './App'
import router from '@/router'
import store from '@/store'
import { CHECK_AUTH } from '@/store/actions.type'

import ApiService from '@/common/api.service'
import DateFilter from '@/common/date.filter'
import ErrorFilter from '@/common/error.filter'
import draggable from 'vuedraggable'
import vSelect from 'vue-select'
import vueSlider from 'vue-slider-component'
import moment from 'moment'
import Paginate from 'vuejs-paginate'
import vmodal from 'vue-js-modal'
import Slick from 'vue-slick'
import { Datetime } from 'vue-datetime'
import { Settings } from 'luxon'
import VueGallerySlideshow from 'vue-gallery-slideshow'


require('../node_modules/slick-carousel/slick/slick.css')
require('../node_modules/vue-datetime/dist/vue-datetime.min.css')


Vue.prototype.moment = moment
Vue.config.productionTip = false

Settings.defaultLocale = 'en'
Vue.use(vmodal, {
  dialog: true,
  dynamic: true,
}) 
Vue.component('vue-gallery-slideshow', VueGallerySlideshow);
Vue.component('datetime', Datetime)
Vue.component('slick', Slick)
Vue.component('paginate', Paginate)
Vue.component('vue-slider', vueSlider)
Vue.component('v-select', vSelect)
Vue.component('draggable', draggable)
Vue.filter('date', DateFilter)
Vue.filter('error', ErrorFilter)

ApiService.init()


router.beforeEach(
  (to, from, next) => {
    return Promise
      .all([store.dispatch(CHECK_AUTH)])
      .then(next)
  }
)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})
