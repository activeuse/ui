import Vue from 'vue'
import { ApiService, ApiAdvertisements } from '@/common/api.service'
import { 
  SET_CATEGORIES, 
  SET_CURRENCIES, 
  SET_RENT_PERIODS,
  SET_COUNTRY_LIST,
  SET_PROFILE_ITEMS,
  SET_ERRORS_ADD_ADV,
  SET_CITY_LIST,
  SET_STATUS_LIST,
  SET_MEASURE_UNITS_LIST,
  SET_GENDER_LIST,
  SET_DELIVERY_TYPES
} from './mutations.type'
import { 
  FETCH_ALL_PROPERTIES,
  FETCH_COUNTRY_LIST,
  FETCH_CITY_LIST,
  ADD_ITEM,
  UPDATE_ITEM,
  FETCH_PROFILE_ITEMS,
  DELETE_ITEM,
  GET_INFO_ITEM_BY_ID,
  SEARCH_ITEMS
} from './actions.type'


const state = {
  categories: [],
  currencies: [],
  rent_periods: [],
  profile_items: [],
  errors_add: [],
  country_list: [],
  city_list: [],
  status: [],
  measureUnits: [],
  gender_list: [],
  delivery_types: []
}

const getters = {
  getAllCategories (state) {
    return state.categories
  },
  getAllCurrencies (state) {
    return state.currencies
  },
  getRentPeriods (state) {
    return state.rent_periods
  },
  getProfileItems (state) {
    return state.profile_items
  },
  getAddErrors (state) {
    return state.errors_add
  },
  getCountryList(state) {
    return state.country_list
  },
  getCityList(state) {
    return state.city_list
  },
  getStatusList(state) {
    return state.status
  },
  getMeasureUnits(state) {
    return state.measureUnits
  },
  getGenderList(state) {
    return state.gender_list
  },
  getDeliveryTypes(state) {
    return state.delivery_types
  }
}

export const actions = {
  [FETCH_ALL_PROPERTIES] (context) {
    return ApiAdvertisements
      .get('/ads/advertisementProperties')
      .then(({data}) => {
        context.commit(SET_CATEGORIES, data.categories)
        context.commit(SET_CURRENCIES, data.currencies)
        context.commit(SET_RENT_PERIODS, data.rentPeriods)
        context.commit(SET_STATUS_LIST, data.advertisementStatus)
        context.commit(SET_MEASURE_UNITS_LIST, data.measureUnits)
        context.commit(SET_GENDER_LIST, data.genders)
        context.commit(SET_DELIVERY_TYPES, data.deliveryTypes)
      })
  },
  [ADD_ITEM](context, params) {
    return ApiAdvertisements
      .post('/ads/advertisements', params)
      .then(({data}) => {
        return data
      })
      .catch(({response}) => {
        var errors = [];
        for(var key in  response.data.ModelState) {
          for (var i = 0; i < response.data.ModelState[key].length; i++) {
            errors.push(response.data.ModelState[key][i]);
          }
        }
        context.commit(SET_ERRORS_ADD_ADV, errors)
      })
  },
  [UPDATE_ITEM](context, params) {
    return ApiAdvertisements
      .put('/ads/advertisements', params)
      .then(({data}) => {
        return data
      })
  },
  [FETCH_PROFILE_ITEMS](context, page) {
    return ApiAdvertisements
      .get('/ads/advertisementsInfo?page=' + page + '&take=6')
      .then(({data}) => {
        context.commit(SET_PROFILE_ITEMS, data)
      })
  },
  [FETCH_COUNTRY_LIST](context) {
    if(window.localStorage.countries != "" && window.localStorage.countries != undefined) {
      var data = JSON.parse(window.localStorage.countries)
      context.commit(SET_COUNTRY_LIST, data)
      return data
    } else {
      return ApiAdvertisements
        .get('/ads/countries')
        .then(({data}) => {
          window.localStorage.countries = JSON.stringify(data)
          context.commit(SET_COUNTRY_LIST, data)
          return data
        })
    }
  },
  [FETCH_CITY_LIST](context, country_id) {
    return ApiAdvertisements
      .get('/ads/cities?countryId=' + country_id)
      .then(({data}) => {
        context.commit(SET_CITY_LIST, data)
        return data
      })
  },
  [DELETE_ITEM](context, item_id) {
    return ApiAdvertisements
      .delete('/ads/advertisements?id=' + item_id)
      .then(({data}) => {
        
      })
  },
  [GET_INFO_ITEM_BY_ID](context, item_id) {
    return ApiAdvertisements
      .get('/ads/advertisements?id=' + item_id)
      .then(({data}) => {
        return data
      })
      .catch((response) => {
        return response
      })
  },
  [SEARCH_ITEMS](context, args) {
    return ApiAdvertisements
      .get('/ads/search?query=' + args.q + '&categoryId=' + args.categoryId + '&cityId=' + args.cityId + '&page=' + args.page + '&take=6')
      .then(({data}) => {
        return data
      })
  }
}

const mutations = {
  [SET_CATEGORIES](state, data) {
    state.categories = data
  },
  [SET_CURRENCIES](state, data) {
    state.currencies = data
  },
  [SET_RENT_PERIODS](state, data) {
    state.rent_periods = data
  },
  [SET_PROFILE_ITEMS](state, data) {
    state.profile_items = data
  },
  [SET_ERRORS_ADD_ADV](state, errors) {
    state.errors_add = errors
  },
  [SET_COUNTRY_LIST](state, data) {
    state.country_list = data
  },
  [SET_CITY_LIST](state, data) {
    state.city_list = data
  },
  [SET_STATUS_LIST](state, data) {
    state.status = data
  },
  [SET_MEASURE_UNITS_LIST](state, data) {
    state.measureUnits = data
  },
  [SET_GENDER_LIST](state, data) {
    state.gender_list = data
  },
  [SET_DELIVERY_TYPES](state, data) {
    state.delivery_types = data
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}