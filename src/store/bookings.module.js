import Vue from 'vue'
import { ApiService, ApiBookings } from '@/common/api.service'

import { 
  SET_ERROR_CREATE_BOOKING
} from './mutations.type'

import { 
  CREATE_BOOKING,
  GET_OUT_BOOKINGS,
  DELETE_OUT_BOOKIGN,
  GET_IN_BOOKINGS,
  DELETE_IN_BOOKIGN,
  APPROVE_BOOKIGN
} from './actions.type'

const state = {
  error_create_booking: ''
}

const getters = {
  getErrorCreateBooking(state) {
    return state.error_create_booking
  }
}

export const actions = {
  [CREATE_BOOKING](context, args) {
    return ApiBookings
      .post('/ads/bookings', args)
      .then(({data}) => {
        return data
      })
      .catch(({response}) => {
        context.commit(SET_ERROR_CREATE_BOOKING, response.data.Message)
      })
  },
  [GET_OUT_BOOKINGS](context) {
    return ApiBookings
      .get('/ads/bookings/outgoing')
      .then(({data}) => {
        return data
      })
  },
  [GET_IN_BOOKINGS](context) {
    return ApiBookings
      .get('/ads/bookings/incoming')
      .then(({data}) => {
        return data
      })
  },
  [DELETE_OUT_BOOKIGN](context, id) {
    return ApiBookings
      .delete('/ads/bookings/outgoing/' + id)
      .then(({data}) => {
        return data
      })
  },
  [DELETE_IN_BOOKIGN](context, id) {
    return ApiBookings
      .delete('/ads/bookings/incoming/' + id + '/reject')
      .then(({data}) => {
        return data
      })
  },
  [APPROVE_BOOKIGN](context, id) {
    return ApiBookings
      .post('/ads/bookings/incoming/' + id + '/approve')
      .then(({data}) => {
        return data
      })
      .catch((response) => {
        return response.response.data.Message
      })
  }
}

const mutations = {
  [SET_ERROR_CREATE_BOOKING](state, data) {
    state.error_create_booking = data
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}