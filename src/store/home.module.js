const state = {
  
  isLoading: true,
}

const getters = {
  
  isLoading (state) {
    return state.isLoading
  },
}


/* eslint no-param-reassign: ["error", { "props": false }] */
const mutations = {
  
}

export default {
  state,
  getters,
  mutations
}
