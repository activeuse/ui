import ApiService from '@/common/api.service'
import JwtService from '@/common/jwt.service'
import { LOGIN, LOGOUT, REGISTER, CHECK_AUTH, UPDATE_USER, UPDATE_PASSWORD } from './actions.type'
import { SET_AUTH, PURGE_AUTH, SET_ERROR, SET_ERROR_SETTINGS1, SET_ERROR_SETTINGS2 } from './mutations.type'


const state = {
  errors: [],
  errors_settings1: [],
  errors_settings2: [],
  user: {},
  isAuthenticated: !!JwtService.getToken()
}

const getters = {
  getErrorSettingsUpdateInfo (state) {
    return state.errors_settings1
  },
  getErrorSettingsUpdatePassword (state) {
    return state.errors_settings2
  },
  currentUser (state) {
    return state.user
  },
  isAuthenticated (state) {
    return state.isAuthenticated
  }
}
function addError(field, data, response) {

  if(response[field] != undefined) {
    for(var i = 0; i < response[field].length; i++) {
      var str = response[field][i]
      str = str.replace("[", '').replace(']', '')
      str = str.trim()
      if(str.length[str.length - 1] == '.') {
        str = str.substring(0, str.length - 1)
      }
      str = str.trim()
      data.push(str)
    }
  }
  return data
}
function addErrorCode(data, response) {
  for(var i = 0; i < response.length; i++) {
    if(response[i].code != undefined) {
      var str = response[i].description
      str = str.replace("[", '').replace(']', '')
      str = str.trim()
      if(str.length[str.length - 1] == '.') {
        str = str.substring(0, str.length - 1)
      }
      str = str.trim()
      data.push(str)  
    }
  }
  return data
}
const actions = {
  [LOGIN] (context, credentials) {
    return new Promise((resolve) => {
      
      ApiService
        .post('/api/accounts/login', credentials)
        .then(({data}) => {
          context.commit(SET_AUTH, data)
          resolve(data)
        })
        .catch(({response}) => {
          var data = [];
          data = addError('Email', data, response.data)
          data = addError('Password', data, response.data)   
          if(data.length == 0) {
            data.push('You entered an incorrect username or password')
          }
          context.commit(SET_ERROR, data)
          return response
        })
    })
  },
  [LOGOUT] (context) {
    context.commit(PURGE_AUTH)
  },
  [REGISTER] (context, credentials) {
    return new Promise((resolve, reject) => {
      ApiService
        .post('/api/accounts/register', credentials)
        .then(({data}) => {
          context.commit(SET_AUTH, data)
          resolve(data)
        })
        .catch(({response}) => {
          var data = [];
          data = addError('Email', data, response.data)
          data = addError('Password', data, response.data)   
          data = addError('FirstName', data, response.data)
          data = addError('LastName', data, response.data)
          data = addError('PhoneNumber', data, response.data)
          data = addErrorCode(data, response.data);
          context.commit(SET_ERROR, data)
        })
    })
  },
  [CHECK_AUTH] (context) {
    if (JwtService.getToken()) {
      ApiService.setHeader()
      ApiService
        .get('/api/userinfo/get')
        .then(({data}) => {
          context.commit(SET_AUTH, data)
        })
        .catch(({response}) => {
          context.commit(PURGE_AUTH)
        })
    } else {
      context.commit(PURGE_AUTH)
    }
  },
  [UPDATE_USER] (context, new_data_user) {
    
    ApiService.setHeader()

    return ApiService
      .put('/api/userinfo/update', new_data_user)
      .then(({data}) => {
        context.commit(SET_AUTH, new_data_user)
        return data
      }).catch(({response}) => {
        var data = [];
        data = addError('FirstName', data, response.data)
        data = addError('LastName', data, response.data)
        data = addError('PhoneNumber', data, response.data)
        data = addErrorCode(data, response.data);
        context.commit(SET_ERROR_SETTINGS1, data)
      })  
  },

  [UPDATE_PASSWORD] (context, passwords) {
    
    ApiService.setHeader()

    return ApiService
      .put('/api/userinfo/password', passwords)
      .then(({data}) => {
        
      }).catch(({response}) => {
        var data = [];
        data = addError('FirstName', data, response.data)
        data = addError('LastName', data, response.data)
        data = addError('PhoneNumber', data, response.data)
        data = addErrorCode(data, response.data);
        context.commit(SET_ERROR_SETTINGS1, data)
      })  
  }
}

const mutations = {
  [SET_ERROR] (state, error) {
    state.errors = error
  },
  [SET_ERROR_SETTINGS1] (state, error) {
    state.errors_settings1 = error
  },
  [SET_ERROR_SETTINGS2] (state, error) {
    state.errors_settings2 = error
  },
  [SET_AUTH] (state, user) {
    state.isAuthenticated = true
    state.user = user
    state.errors = {}
    JwtService.saveToken(user.access_token)
  },
  [PURGE_AUTH] (state) {
    state.isAuthenticated = false
    state.user = {}
    state.errors = {}
    JwtService.destroyToken()
  }
}

export default {
  state,
  actions,
  mutations,
  getters
}
