import Vue from 'vue'
import Vuex from 'vuex'

import home from './home.module'
import auth from './auth.module'
import advertisements from './advertisements.module'
import wallets from './wallets.module'
import bookings from './bookings.module'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    home,
    auth,
    advertisements,
    wallets,
    bookings
  }
})
