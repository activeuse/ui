import Vue from 'vue'
import { ApiService, ApiWallets } from '@/common/api.service'

/*import { 

} from './mutations.type'*/

import { 
  ADD_WALLET,
  GET_WALLETS,
  ACTIVATE_WALLET,
  DEACTIVATE_WALLET
} from './actions.type'

const state = {

}

const getters = {

}

export const actions = {
  [ADD_WALLET] (context, args) {
    return ApiWallets
      .post('/evt/evtwallets', args)
      .then(({data}) => {
        return data
      })
      .catch(({response}) => {
        return response
      })
  },
  [GET_WALLETS] (context) {
    return ApiWallets
      .get('/evt/evtWallets')
      .then(({data}) => {
        return data
      })
      .catch(({response}) => {
        return response
      })
  },
  [ACTIVATE_WALLET] (context, id) {
    return ApiWallets
      .put('/evt/evtWallets/activate?walletId=' + id)
      .then(({data}) => {
        return data
      })
      .catch(({response}) => {
        return response
      })
  },
  [DEACTIVATE_WALLET] (context, id) {
    return ApiWallets
      .put('/evt/evtWallets/deactivate?walletId=' + id)
      .then(({data}) => {
        return data
      })
      .catch(({response}) => {
        return response
      })
  }
}

const mutations = {

}

export default {
  state,
  getters,
  actions,
  mutations
}