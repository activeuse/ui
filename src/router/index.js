import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '',
      name: 'home1',
      component: () => import('@/views/Home')
    },
    {
      path: '/',
      name: 'home',
      component: () => import('@/views/Home')
    },
    {
      name: 'login',
      path: '/login',
      component: () => import('@/views/Login')
    },
    {
      name: 'register',
      path: '/registration',
      component: () => import('@/views/Register')
    },
    {
      name: 'settings',
      path: '/profile/settings',
      component: () => import('@/views/Settings')
    },
    {
      name: 'profile',
      path: '/profile',
      component: () => import('@/views/Settings')
    },
    {
      name: 'profile_messages',
      path: '/profile/messages',
      component: () => import('@/views/Settings')
    },
    {
      name: 'profile_items',
      path: '/advertisements',
      component: () => import('@/views/ProfileItems')
    },
    {
      name: 'items_add',
      path: '/advertisements/add',
      component: () => import('@/views/AddAdv')
    },
    {
      name: 'profile_item_edit',
      path: '/advertisements/edit/:item_id',
      component: () => import('@/views/AddAdv')
    },
    {
      name: 'item_page',
      path: '/advertisements/:item_id',
      component: () => import('@/views/Item')
    },
    {
      name: 'search_page',
      path: '/search',
      component: () => import('@/views/Search'),
      props: (route) => ({ q: route.query.q })
    },
    {
      name: 'wallets_page',
      path: '/wallets',
      component: () => import('@/views/Wallets')
    },
    {
      name: 'booking_page',
      path: '/bookings',
      component: () => import('@/views/Bookings'),
      children: [
        {
          name: 'incoming_booking_page',
          path: '/bookings/incoming',
          component: () => import('@/views/Bookings')
        },
        {
          name: 'outgoing_booking_page',
          path: '/bookings/outgoing',
          component: () => import('@/views/Bookings')
        }
      ]
    }/*,
    {
      name: 'about',
      path: '/about',
      component: () => import('@/views/About'),
    }*/
  ],
  mode: 'history'
})
